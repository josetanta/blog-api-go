package main

import (
	"fmt"
	"gitlab.com/josetanta/blog-api/middlewares"
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/josetanta/blog-api/migrate"
	"gitlab.com/josetanta/blog-api/routes"
	utilsapp "gitlab.com/josetanta/blog-api/utils"
)

func init() {
	fmt.Println("Init Application")
	utilsapp.LoadEnvironment()
}

func main() {

	migrate.DataBaseMigrate()

	app := fiber.New()

	middlewares.SetupMiddleware(app)

	routes.SetupRoutesAPI(app)

	var p string
	if os.Getenv("PORT") == "" {
		p = "5000"
	} else {
		p = os.Getenv("PORT")
	}
	port := fmt.Sprintf(":%s", p)
	log.Fatal(app.Listen(port))
}
