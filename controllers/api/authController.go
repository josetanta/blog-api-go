package v1

import (
	"os"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/shareed2k/goth_fiber"
	"gitlab.com/josetanta/blog-api/database"
	"gitlab.com/josetanta/blog-api/forms"
	"gitlab.com/josetanta/blog-api/models"
	"gitlab.com/josetanta/blog-api/utils"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

var (
	RedirectCallbackClient      string
	RedirectCallbackClientError string
)

func loadingEnv() {
	utils.LoadEnvironment()
	RedirectCallbackClient = os.Getenv("REDIRECT_CALLBACK_CLIENT")
	RedirectCallbackClientError = os.Getenv("REDIRECT_CALLBACK_CLIENT_ERROR")
}

func UserLogin(ctx *fiber.Ctx) error {
	data := new(forms.UserForm)

	if err := ctx.BodyParser(data); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "No se puede procesar este objeto.",
		})
	}

	var user, existUser = new(models.User).GetObject(map[string]string{"email": data.Email})

	if user.Signed {
		ctx.Cookie(&fiber.Cookie{
			Name:     "jwt",
			Value:    "",
			Expires:  time.Now().Add(-time.Hour),
			HTTPOnly: true,
		})
		user.DeleteToken()

	} else {
		user.SetSigned(true)
	}

	if !existUser {
		return ctx.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"message": "Este usuario no se encuentra registrado.",
		})
	} else if err := bcrypt.CompareHashAndPassword(user.Password, []byte(data.Password)); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Las credenciales son incorrectas.",
		})
	}

	token, _ := models.GenerateToken(user.ID)

	user.SaveUserToken(token)

	ctx.Cookie(&fiber.Cookie{
		Name:     "jwt",
		Value:    token,
		Expires:  utils.ExpiredIn,
		SameSite: "Lax",
		HTTPOnly: true,
	})

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"message": "Inicio Sesión exitosamente.",
		"token":   token,
	})
}

func UserRegister(ctx *fiber.Ctx) error {
	var userForm = new(forms.UserForm)

	if err := ctx.BodyParser(userForm); err != nil {
		return err
	}

	errors := forms.ValidateUserForm(*userForm)

	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": errors,
		})
	}

	var userLogged, existUser = new(models.User).GetObject(map[string]string{"email": userForm.Email, "username": userForm.Username})

	if userLogged.Signed {
		return ctx.Status(fiber.StatusPermanentRedirect).JSON(fiber.Map{
			"message": "Usted ya esta registrado.",
		})
	} else if existUser {
		return ctx.Status(fiber.StatusConflict).JSON(fiber.Map{
			"message": "Este usuario ya existe, por favor intente nuevamente.",
		})
	}

	password, _ := bcrypt.GenerateFromPassword([]byte(userForm.Password), 14)

	database.DB.Create(&models.User{
		Username: userForm.Username,
		Email:    userForm.Email,
		Password: password,
		RoleID:   1,
	})

	return ctx.Status(fiber.StatusCreated).JSON(fiber.Map{
		"message": "Usted se acaba de registrar",
	})
}

func UserLogout(ctx *fiber.Ctx) error {

	token := utils.GetToken(ctx)
	var _, user, existToken = new(models.UserToken).GetObject(token)

	if existToken {
		user.SetSigned(false)
		user.DeleteToken()
		cookie := fiber.Cookie{
			Name:     "jwt",
			Value:    "",
			Expires:  time.Now().Add(-time.Hour),
			Secure:   true,
			HTTPOnly: true,
		}

		ctx.Cookie(&cookie)
		return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
			"message": "Cerro sesión exitosamente.",
		})
	}
	return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
		"message": "Error al cerrar sesión",
	})

}

// Login with Providers

func LoginAuth(ctx *fiber.Ctx) error {
	if user, err := goth_fiber.CompleteUserAuth(ctx); err == nil {
		return ctx.JSON(user.AccessToken)
	} else {
		return goth_fiber.BeginAuthHandler(ctx)
	}
}

func LoginAuthUser(ctx *fiber.Ctx) error {
	loadingEnv()
	user, err := goth_fiber.CompleteUserAuth(ctx)

	if err != nil {

		return ctx.Redirect(RedirectCallbackClientError)

	} else {

		password, _ := bcrypt.GenerateFromPassword([]byte(user.UserID), 14)

		userId, _ := strconv.Atoi(user.UserID)

		var userSaved = models.User{
			Username: user.NickName,
			Email:    user.Email,
			RoleID:   1,
			Password: password,
			Model: gorm.Model{
				ID: uint(userId),
			},
		}

		var getUser, existGetUser = new(models.User).GetObject(map[string]string{"userId": user.UserID})
		var token string

		if !existGetUser {

			database.DB.Save(&userSaved).Commit()

			userSaved.SetSigned(true)

			var profileUser, _ = new(models.Profile).GetObject(userSaved.ID)

			token, _ = models.GenerateToken(userSaved.ID)
			// Update Thumbnail
			database.DB.
				Model(&profileUser).
				Where("user_id = ?", userSaved.ID).
				UpdateColumn("thumbnail", user.AvatarURL).
				Commit()

		} else {
			ctx.ClearCookie()
			getUser.DeleteToken()
			getUser.SetSigned(true)
			token, _ = models.GenerateToken(getUser.ID)
		}

		userSaved.SaveUserToken(token)
		ctx.Cookie(&fiber.Cookie{
			Name:     "jwt",
			Value:    token,
			Expires:  utils.ExpiredIn,
			Secure:   true,
			HTTPOnly: true,
		})
		ctx.JSON(fiber.Map{
			"token": token,
		})
		return ctx.Redirect(RedirectCallbackClient)
	}

}

func GetTokenUser(ctx *fiber.Ctx) error {
	token := utils.GetToken(ctx)
	return ctx.JSON(fiber.Map{
		"token": token,
	})
}

func LogoutAuth(ctx *fiber.Ctx) error {
	token := utils.GetToken(ctx)

	var _, user, existToken = new(models.UserToken).GetObject(token)

	if err := goth_fiber.Logout(ctx); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON("Error")
	}

	if existToken {
		user.SetSigned(false)
		user.DeleteToken()

		ctx.Cookie(&fiber.Cookie{
			Name:     "jwt",
			Value:    "",
			Expires:  time.Now().Add(-time.Hour),
			Secure:   true,
			HTTPOnly: true,
		})
	}

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"message": "Cerro sesión exitosamente.",
	})

}
