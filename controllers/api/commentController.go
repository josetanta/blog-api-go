package v1

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/josetanta/blog-api/database"
	"gitlab.com/josetanta/blog-api/forms"
	"gitlab.com/josetanta/blog-api/models"
	"gitlab.com/josetanta/blog-api/utils"
)

func CreateComment(ctx *fiber.Ctx) error {
	postId := ctx.Params("postId")
	token := utils.GetToken(ctx)
	var commentForm = new(forms.CommentForm)

	if err := ctx.BodyParser(commentForm); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Por favor corrige los errores.",
		})
	}

	errors := forms.ValidateCommentForm(*commentForm)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Por favor corrige los errores.",
		})
	}

	var _, user, _ = new(models.UserToken).GetObject(token)
	var post, _ = new(models.Post).GetObject(postId)

	var newComment = models.Comment{
		PostID:  post.ID,
		UserID:  user.ID,
		Content: commentForm.Content,
	}

	database.DB.Create(&newComment).Commit()

	return ctx.Status(fiber.StatusCreated).JSON(fiber.Map{
		"message": "Su comentario a sido creado.",
		"comment": newComment.ToJSON(ctx),
	})
}

func UpdateComment(ctx *fiber.Ctx) error {
	commentId := ctx.Params("commentId")
	var commentForm = new(forms.CommentForm)
	if err := ctx.BodyParser(commentForm); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Por favor corrige los errores.",
		})
	}

	errors := forms.ValidateCommentForm(*commentForm)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(errors)
	}

	var comment, _ = new(models.Comment).GetObject(map[string]string{"commentId": commentId})

	database.DB.
		Model(&comment).
		Where("id = ?", comment.ID).
		UpdateColumns(models.Comment{Content: commentForm.Content}).
		Commit()

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"message": "Su comentario a sido actualizado.",
		"comment": comment.ToJSON(ctx),
	})
}

func DeleteComment(ctx *fiber.Ctx) error {
	commentId := ctx.Params("commentId")

	var comment, _ = new(models.Comment).GetObject(map[string]string{"commentId": commentId})

	database.DB.Model(&comment).Update("deleted", true).Commit()

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"message": "Su comentario fue eliminado.",
	})
}
