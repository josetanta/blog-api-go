package v1

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/josetanta/blog-api/database"
	"gitlab.com/josetanta/blog-api/forms"
	"gitlab.com/josetanta/blog-api/models"
	"gitlab.com/josetanta/blog-api/utils"
)

func GetPostList(ctx *fiber.Ctx) error {
	posts := models.PostGetObjectsAll(ctx, "created_at desc")
	return ctx.JSON(fiber.Map{
		"posts":       posts,
		"total_posts": len(posts),
	})
}

func GetPostSingle(ctx *fiber.Ctx) error {

	postId := ctx.Params("postId", "1")

	var post, existPost = new(models.Post).GetObject(postId)

	if !existPost {
		return ctx.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"message": "La publicación no existe.",
		})
	}

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"post": post.ToJSON(ctx),
	})
}

func CreatePost(ctx *fiber.Ctx) error {
	token := utils.GetToken(ctx)
	var _, user, existToken = new(models.UserToken).GetObject(token)
	postForm := new(forms.PostForm)

	if err := ctx.BodyParser(postForm); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	errors := forms.ValidatePostForm(*postForm)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(errors)
	}

	nameFileImage, _ := utils.SaveFile(ctx, "posts")

	newPost := models.Post{
		UserID:  user.ID,
		Content: postForm.Content,
		Title:   postForm.Title,
		Image:   nameFileImage,
	}

	if existToken {
		database.DB.Create(&newPost)
	}

	return ctx.Status(fiber.StatusCreated).JSON(fiber.Map{
		"message": "Usted acaba de publicar su publicación",
		"post":    newPost.ToJSON(ctx),
	})
}

func UpdatePost(ctx *fiber.Ctx) error {
	var postId = ctx.Params("postId")
	var postForm = new(forms.PostForm)

	if err := ctx.BodyParser(postForm); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	var updatePost, _ = new(models.Post).GetObject(postId)

	newImage, exist := utils.SaveFile(ctx, "posts")

	if utils.RemoveFileStatic("posts", updatePost.Image) || exist {
		postForm.Image = newImage
	}

	database.DB.Model(&updatePost).
		UpdateColumns(models.Post{
			Image:   postForm.Image,
			Content: postForm.Content,
			Title:   postForm.Title,
		}).Commit()

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"message": "Se actualizo la publicación " + updatePost.Title,
		"post":    updatePost.ToJSON(ctx),
	})
}

func DeletePost(ctx *fiber.Ctx) error {
	var postId = ctx.Params("postId")
	var postDelete, _ = new(models.Post).GetObject(postId)

	utils.RemoveFileStatic("posts", postDelete.Image)
	database.DB.Delete(&postDelete).Commit()

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"message": "Elimino su publicación",
	})
}

func SearchPosts(ctx *fiber.Ctx) error {
	fieldQuery := ctx.Query("q", "Mi post")
	orderDate := ctx.Query("order_date", "desc")
	limit := ctx.Query("limit", "5")

	results := models.PostGetObjectsSearch(ctx, limit, fieldQuery, orderDate)

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"posts":       results,
		"total_posts": len(results),
	})
}

func PostDeletedForUser(ctx *fiber.Ctx) error {
	token := utils.GetToken(ctx)
	method := strings.ToLower(ctx.Method())
	var _, user, _ = new(models.UserToken).GetObject(token)

	postId, deleted := ctx.Query("post_id"), ctx.Query("deleted")
	var getPost, existGetPost = new(models.Post).GetObject(postId)

	if method == "get" {
		postsDeleted := models.PostGetObjectsDeletedForUser(ctx, user.ID)

		return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
			"post":        postsDeleted,
			"total_posts": len(postsDeleted),
		})
	} else if existGetPost && (method == "post" || method == "delete") {

		message := ""

		switch method {
		case "post":
			message = "La publicación se acaba de deshacer de la papelera."
		case "delete":
			message = "La publicación se movió a la papelera."
		}

		database.DB.Model(&getPost).UpdateColumn("deleted", deleted).Commit()
		return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
			"message":    message,
			"post_title": getPost.Title,
			"post_id":    getPost.ID,
		})

	}
	return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
		"message": "Esta publicación no existe.",
	})
}
