package v1

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/josetanta/blog-api/database"
	"gitlab.com/josetanta/blog-api/forms"
	"gitlab.com/josetanta/blog-api/models"
	"gitlab.com/josetanta/blog-api/utils"
)

func GetUserAccount(ctx *fiber.Ctx) error {
	token := utils.GetToken(ctx)

	var _, user, existUser = new(models.UserToken).GetObject(token)
	var account = new(models.Account).GetObject(user.ID)

	if !existUser {
		ctx.Status(fiber.StatusNotFound)
		return ctx.JSON(fiber.Map{
			"message": "Este usuario no esta registrado.",
		})
	}

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"user": account.ToJSON(ctx),
	})
}

func GetUserDetails(ctx *fiber.Ctx) error {
	userId := ctx.Params("userId")
	var user, existUser = new(models.User).GetObject(map[string]string{"userId": userId, "uuid": userId})

	if !existUser {
		ctx.Status(fiber.StatusNotFound)
		return ctx.JSON(fiber.Map{
			"message": "Este usuario no esta registrado.",
		})
	}

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"user": user.ToJSON(ctx),
	})
}

func UpdateUserAccount(ctx *fiber.Ctx) error {
	var userProfileForm = new(forms.UserProfileForm)

	if err := ctx.BodyParser(userProfileForm); err != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Error con los campos.",
		})
	}

	errors := forms.ValidateUserProfileForm(*userProfileForm)
	if errors != nil {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": errors,
		})
	}

	token := utils.GetToken(ctx)
	var _, user, existedUserToken = new(models.UserToken).GetObject(token)

	if !existedUserToken {
		return ctx.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"message": "Este usuario no esta registrado.",
		})
	}

	var profile, _ = new(models.Profile).GetObject(user.ID)
	newFileSave, existFile := utils.SaveFile(ctx, "users")

	if utils.RemoveFileStatic("users", profile.Thumbnail) || existFile {
		userProfileForm.Thumbnail = newFileSave
	}

	models.UpdateAccount(user, profile, *userProfileForm)

	var account = new(models.Account).GetObject(user.ID)

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"message": "Usted acaba de actualizar su perfil",
		"user":    account.ToJSON(ctx),
	})
}

func DeleteAccount(ctx *fiber.Ctx) error {
	token := utils.GetToken(ctx)
	var _, user, existUserToken = new(models.UserToken).GetObject(token)

	if !existUserToken && user.ID != 0 {
		return ctx.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Este usuario no existe",
		})
	}

	database.DB.Delete(&user).Commit()

	cookie := fiber.Cookie{
		Name:    "jwt",
		Value:   "",
		Expires: time.Now().Add(-time.Hour),
	}
	ctx.Cookie(&cookie)

	return ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"message": "Se procedió a eliminar su cuenta",
		"delete":  "ok",
	})

}
