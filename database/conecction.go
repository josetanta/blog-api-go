package database

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/josetanta/blog-api/utils"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func getTypeConnection() gorm.Dialector {
	utils.LoadEnvironment()
	env := os.Getenv("APP_ENV")

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s", os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_NAME"), os.Getenv("DB_PORT"), os.Getenv("DB_SSL_MODE"))

	if strings.ToLower(os.Getenv("DB_DRIVER")) == "psql" {
		return postgres.New(postgres.Config{
			DSN:                  dsn,
			PreferSimpleProtocol: true,
		})
	} else {
		if env == "" || env == "local" {
			return sqlite.Open("./database/dev-site.sqlite")
		}
		return sqlite.Open("./database/" + os.Getenv("DB_NAME") + ".sqlite")
	}

}

func connectDB() *gorm.DB {

	connection, err := gorm.Open(getTypeConnection(), &gorm.Config{
		QueryFields: true,
	})

	if err != nil {
		panic("No se pudo conectar con la base de datos.")
	}

	return connection
}

var DB = connectDB()
