package forms

import "github.com/go-playground/validator/v10"

type PostForm struct {
	Title   string `validate:"required,min=5,max=32" json:"title" xml:"title" form:"title"`
	Content string `validate:"required" json:"content" xml:"content" form:"content"`
	Image   string `json:"image" xml:"image" form:"image"`
}

func ValidatePostForm(form PostForm) []*ErrorResponse {
	var errors []*ErrorResponse
	validate := validator.New()
	err := validate.Struct(form)

	if err != nil {
		for _, e := range err.(validator.ValidationErrors) {
			var obj ErrorResponse
			obj.Field = e.Field()
			obj.Tag = e.Tag()
			obj.Value = e.Param()
			obj.ErrorMessage = e.Error()
			errors = append(errors, &obj)
		}
	}
	return errors
}

type CommentForm struct {
	Content string `validate:"required" json:"content" xml:"content" form:"content"`
}

func ValidateCommentForm(form CommentForm) []*ErrorResponse {
	var errors []*ErrorResponse
	validate := validator.New()
	err := validate.Struct(form)

	if err != nil {
		for _, e := range err.(validator.ValidationErrors) {
			var obj ErrorResponse
			obj.Field = e.Field()
			obj.Tag = e.Tag()
			obj.Value = e.Param()
			obj.ErrorMessage = e.Error()
			errors = append(errors, &obj)
		}
	}
	return errors
}
