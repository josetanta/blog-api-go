package forms

import "github.com/go-playground/validator/v10"

type UserForm struct {
	Email          string `validate:"required,email" json:"email" xml:"email" form:"email"`
	Username       string `validate:"required" json:"username" xml:"username" form:"username"`
	Password       string `validate:"required" json:"password" xml:"password" form:"password"`
	RepeatPassword string `validate:"required,min=6,max=32,eq=password" json:"repeatPassword" xml:"repeatPassword" form:"repeatPassword"`
}

type UserProfileForm struct {
	Email     string `validate:"required,email" json:"email" xml:"email" form:"email"`
	Username  string `validate:"required" json:"username" xml:"username" form:"username"`
	Thumbnail string `json:"thumbnail" xml:"thumbnail" form:"thumbnail"`
	Phone     string `validate:"number,min=9,max=9" json:"phone" xml:"phone" form:"phone"`
	Address   string `json:"address" xml:"address" form:"address"`
	Address2  string `json:"address_2" xml:"address_2" form:"address_2"`
}

func ValidateUserForm(form UserForm) []*ErrorResponse {
	var errors []*ErrorResponse
	validate := validator.New()
	err := validate.Struct(form)

	if err != nil {
		for _, e := range err.(validator.ValidationErrors) {
			var field ErrorResponse
			field.Field = e.Field()
			field.Tag = e.Tag()
			field.Value = e.Param()
			errors = append(errors, &field)
		}
	}
	return errors
}

func ValidateUserProfileForm(form UserProfileForm) []*ErrorResponse {
	var errors []*ErrorResponse
	validate := validator.New()
	err := validate.Struct(form)

	if err != nil {
		for _, e := range err.(validator.ValidationErrors) {
			var field ErrorResponse
			field.Field = e.Field()
			field.Tag = e.Tag()
			field.Value = e.Param()
			errors = append(errors, &field)
		}
	}
	return errors
}
