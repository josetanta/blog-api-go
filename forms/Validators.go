package forms

type ErrorResponse struct {
	Field        string `json:"failed_field"`
	Tag          string `json:"tag"`
	Value        string `json:"value"`
	ErrorMessage string `json:"error_message"`
}
