package middlewares

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/basicauth"
	"gitlab.com/josetanta/blog-api/models"
	"gitlab.com/josetanta/blog-api/utils"
)

var PermissionMiddleware = basicauth.Config{

	Unauthorized: func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusForbidden).JSON(fiber.Map{
			"message": "Usted no cuenta con suficientes permisos.",
		})
	},
	Realm: "Forbidden",
	Next: func(ctx *fiber.Ctx) bool {

		token := utils.GetToken(ctx)
		var _, user, exist = new(models.UserToken).GetObject(token)
		return exist && user.HasToken(token) && user.Can(utils.COMMENT|utils.WRITE|utils.FOLLOW)
	},
}

var HasTokenMiddleware = basicauth.Config{

	Unauthorized: func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": "Usted no esta autorizado.",
		})
	},
	Realm: "Unauthorized",
	Next: func(ctx *fiber.Ctx) bool {
		token := utils.GetToken(ctx)

		if len(token) <= 0 {
			return false
		}

		verifyToken := models.VerifyToken(token)
		var userToken, user, existedUserToken = new(models.UserToken).GetObject(token)
		verifiedExpiredToken := utils.IsExpiredVerify(userToken.ExpiredIn)

		if (!verifyToken || verifiedExpiredToken) && user.Signed {
			user.SetSigned(false)
			user.DeleteToken()
			ctx.ClearCookie()
			return false
		} else {
			return existedUserToken && verifyToken && user.Signed
		}
	},
}

var VerifyOwnerPostMiddleware = basicauth.Config{
	Unauthorized: func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusForbidden).JSON(fiber.Map{
			"message": "Usted no es el autor de esta publicación.",
		})
	},
	Authorizer: func(account, password string) bool {
		return true
	},
	Realm: "Forbidden",
	Next: func(ctx *fiber.Ctx) bool {
		token := utils.GetToken(ctx)
		postId := ctx.Params("postId")
		var _, user, _ = new(models.UserToken).GetObject(token)
		return validPostOwner(user, postId)
	},
}

var VerifyOwnerCommentMiddleware = basicauth.Config{
	Unauthorized: func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusForbidden).JSON(fiber.Map{
			"message": "Usted no es el autor de este comentario.",
		})
	},
	Authorizer: func(account, password string) bool {
		return true
	},
	Realm: "Forbidden",
	Next: func(ctx *fiber.Ctx) bool {
		token := utils.GetToken(ctx)
		commentId := ctx.Params("commentId")
		var _, user, _ = new(models.UserToken).GetObject(token)
		return validCommentOwner(user, commentId)
	},
}

func validPostOwner(user models.User, postId string) bool {
	var post, existPost = new(models.Post).GetObject(postId)

	if !existPost {
		return false
	} else {
		return post.UserID == user.ID
	}
}

func validCommentOwner(user models.User, commentId string) bool {
	var comment, existComment = new(models.Comment).GetObject(map[string]string{"commentId": commentId})
	if !existComment {
		return false
	}
	return comment.UserID == user.ID

}
