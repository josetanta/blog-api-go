package middlewares

import (
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/csrf"
	"github.com/gofiber/fiber/v2/middleware/encryptcookie"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/utils"
)

func SetupMiddleware(app *fiber.App) {

	app.Use(encryptcookie.New(encryptcookie.Config{
		Key: encryptcookie.GenerateKey(),
	}))

	app.Use(cors.New(cors.Config{
		AllowCredentials: false,
		AllowOrigins:     os.Getenv("ALLOW_ORIGINS"),
		AllowHeaders:     "Origin, Content-Type, Accept, Authorization",
	}))

	app.Use(csrf.New(csrf.Config{
		KeyLookup:      "cookie:csrf_token",
		CookieName:     "csrf_token",
		KeyGenerator:   utils.UUID,
		CookieHTTPOnly: true,
		CookieDomain:   os.Getenv("CSRF_DOMAINS"),
	}))

	app.Use(logger.New())
}
