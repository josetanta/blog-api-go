package migrate

import (
	"gitlab.com/josetanta/blog-api/database"
	"gitlab.com/josetanta/blog-api/models"
)

func DataBaseMigrate() {
	connection := database.DB

	err := connection.AutoMigrate(
		&models.Role{},
		&models.User{},
		&models.Post{},
		&models.Comment{},
		&models.Profile{},
		&models.UserToken{},
		&models.Like{},
	)

	if err != nil {

		panic("Los modelos son incorrectos.")
	}
	new(models.Role).GenerateRoles()
}
