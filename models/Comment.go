package models

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/josetanta/blog-api/database"
	"gitlab.com/josetanta/blog-api/utils"
	"gorm.io/gorm"
)

type (
	IComment interface {
		GetObject() Comment
		GetObjectAll(postId string) []Comment
	}

	Comment struct {
		gorm.Model `json:"-"`
		Content    string `json:"content"`
		Published  bool   `gorm:"default:true" json:"published"`
		Deleted    bool   `gorm:"default:false"`
		// User
		UserID uint `json:"user_id"`
		// User   `gorm:"constraint:onDelete:CASCADE,onUpdate:CASCADE;" json:"-"`

		// Post
		PostID uint `json:"post_id"`
		// Post   `gorm:"constraint:onDelete:CASCADE,onUpdate:CASCADE;" json:"-"`

		// Likes
		Likes []Like `gorm:"foreignKey:CommentID" json:"likes"`
	}
)

func (comment *Comment) GetObject(params map[string]string) (Comment, bool) {
	var exist = false

	userId, _ := strconv.Atoi(params["userId"])
	if userId != 0 {
		database.DB.Where("user_id = ?", userId).First(comment)
	}

	postId, _ := strconv.Atoi(params["postId"])
	if postId != 0 {
		database.DB.Where("post_id = ?", postId).First(comment)
	}

	commentId, _ := strconv.Atoi(params["commentId"])
	if commentId != 0 {
		database.DB.Where("id = ?", commentId).First(comment)
	}

	if comment.ID != 0 {
		exist = true
	}

	return *comment, exist
}

func CommentOfPostGetObjectAll(ctx *fiber.Ctx, id uint) []fiber.Map {
	var searchComments []Comment

	database.DB.Where("post_id = ? and deleted = FALSE", id).Order("created_at desc").Find(&searchComments)
	var result = make([]fiber.Map, 0, len(searchComments))

	for _, c := range searchComments {
		result = append(result, c.ToJSON(ctx))
	}

	return result
}

func CommentGetObjectsByUserId(ctx *fiber.Ctx, id uint, params ...string) []fiber.Map {
	var searchComments []Comment
	database.DB.Find(&searchComments).Where("user_id = ? and deleted = FALSE", id).Order(params)
	var result = make([]fiber.Map, 0, len(searchComments))

	for _, c := range searchComments {
		result = append(result, c.ToJSON(ctx))
	}

	return result
}

func (comment *Comment) ToJSON(ctx *fiber.Ctx) fiber.Map {

	var post, _ = new(Post).GetObject(strconv.Itoa(int(comment.PostID)))
	var user = new(Account).GetObject(comment.UserID)

	return fiber.Map{
		"id":         comment.ID,
		"content":    comment.Content,
		"published":  comment.Published,
		"created_at": comment.CreatedAt,
		"relations": fiber.Map{
			"user": fiber.Map{
				"id":        user.ID,
				"username":  user.Username,
				"email":     user.Email,
				"thumbnail": utils.GetFileStatic(ctx, "users", user.Thumbnail),
			},
			"post": fiber.Map{
				"id":    post.ID,
				"title": post.Title,
			},
		},
	}
}
