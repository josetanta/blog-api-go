package models

import "gorm.io/gorm"

type Like struct {
	gorm.Model   `json:"-"`
	LikeCount    int `gorm:"default:0" json:"like_count"`
	DislikeCount int `gorm:"default:0" json:"dislike_count"`

	UserID    uint `json:"user_id"`
	PostID    uint `json:"post_id"`
	CommentID uint `json:"comment_id"`
}
