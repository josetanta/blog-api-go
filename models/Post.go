package models

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"gitlab.com/josetanta/blog-api/database"
	utils2 "gitlab.com/josetanta/blog-api/utils"
	"gorm.io/gorm"
)

type Post struct {
	gorm.Model `json:"-"`
	Title      string `json:"title"`
	Content    string `json:"content"`
	Published  bool   `gorm:"default:true" json:"published"`
	Deleted    bool   `gorm:"default:false" json:"deleted"`
	Image      string `gorm:"default:'image.jpg'" json:"image"`
	UUID       string `json:"uuid" gorm:"index:idx_uuid_post,unique"`

	// User - Author
	UserID uint `json:"user_id"`
	// User   `gorm:"constraint:onDelete:CASCADE,onUpdate:CASCADE;" json:"-"`

	// Comments
	Comments []Comment `gorm:"foreignKey:PostID" json:"comments"`
}

func (post *Post) BeforeCreate(_ *gorm.DB) (err error) {
	post.UUID = utils.UUIDv4()
	return
}

func (post *Post) GetObject(postId string) (Post, bool) {
	var exist = false

	id, _ := strconv.Atoi(postId)

	if id > 0 {
		database.DB.Where("id = ?", id).First(post)
	}

	if post.ID != 0 {
		exist = true
	}

	return *post, exist
}

func PostGetObjectsByUserId(ctx *fiber.Ctx, id uint, params ...string) []fiber.Map {
	var searchPosts []Post

	database.DB.Where("user_id = ? AND deleted = FALSE AND published = TRUE", id).Find(&searchPosts).Order(params)
	var result = make([]fiber.Map, 0, len(searchPosts))
	for _, post := range searchPosts {
		result = append(result, post.ToJSON(ctx))
	}
	return result
}

func PostGetObjectsAll(ctx *fiber.Ctx, orderParams ...string) []fiber.Map {
	var searchPosts []Post
	var paramsQuery string

	for _, text := range orderParams {
		paramsQuery += text + ","
	}

	paramsQuery = paramsQuery[0 : len(paramsQuery)-1]

	database.DB.
		Where("published = TRUE").
		Where("deleted = FALSE").
		Order(paramsQuery).
		Find(&searchPosts)

	var result = make([]fiber.Map, 0, len(searchPosts))

	for _, post := range searchPosts {
		result = append(result, post.ToJSON(ctx))
	}
	return result
}

func (post *Post) ToJSON(ctx *fiber.Ctx) fiber.Map {
	var account = new(Account).GetObject(post.UserID)

	imagePostUrl := utils2.GetFileStatic(ctx, "posts", post.Image)
	imageUserUrl := utils2.GetFileStatic(ctx, "users", account.Thumbnail)

	return fiber.Map{
		"id":      post.ID,
		"title":   post.Title,
		"content": post.Content,
		"image": fiber.Map{
			"url_image":  imagePostUrl,
			"name_image": post.Image,
		},
		"created_at": post.CreatedAt,
		"post_uuid":  post.UUID,
		"relations": fiber.Map{
			"user": fiber.Map{
				"id":        account.ID,
				"username":  account.Username,
				"email":     account.Email,
				"thumbnail": imageUserUrl,
			},
			"comments": CommentOfPostGetObjectAll(ctx, post.ID),
		},
	}
}

func PostGetObjectsSearch(ctx *fiber.Ctx, params ...string) []fiber.Map {
	var searchPosts []Post

	limit, _ := strconv.Atoi(params[0])
	fieldQuery, orderDate := params[1], params[2]

	database.DB.
		Limit(limit).
		Where("title LIKE ?", "%"+fieldQuery+"%").
		Or("content LIKE ?", "%"+fieldQuery+"%").
		Order("created_at "+orderDate).
		Find(&searchPosts, "deleted = 0")

	var result = make([]fiber.Map, 0, len(searchPosts))

	for _, post := range searchPosts {
		result = append(result, post.ToJSON(ctx))
	}
	return result
}

func PostGetObjectsDeletedForUser(ctx *fiber.Ctx, id uint) []fiber.Map {
	var searchPosts []Post

	database.DB.
		Where("user_id = ?", id).
		Find(&searchPosts, "deleted = 1")

	var result = make([]fiber.Map, 0, len(searchPosts))

	for _, post := range searchPosts {
		result = append(result, post.ToJSON(ctx))
	}
	return result
}
