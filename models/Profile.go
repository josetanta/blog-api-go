package models

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/josetanta/blog-api/database"
	"gitlab.com/josetanta/blog-api/forms"
	"gitlab.com/josetanta/blog-api/utils"
)

type Profile struct {
	ID        uint   `gorm:"primaryKey" json:"id"`
	Thumbnail string `gorm:"default:'thumbnail.png'" json:"thumbnail"`
	Phone     string `gorm:"default:''" json:"phone"`
	Address   string `gorm:"default:''" json:"address"`
	Address2  string `gorm:"default:''" json:"address_2"`

	UserID uint `gorm:"foreignKey:UserID" json:"user_id"`
	//User   `gorm:"constraint:onDelete:CASCADE,onUpdate:CASCADE;" json:"-"`
}

func (profile *Profile) GetObject(userId uint) (Profile, bool) {
	var exist = false

	if userId > 0 {
		database.DB.Where("user_id = ?", userId).First(profile)
	}
	if profile.ID != 0 {
		exist = true
	}
	return *profile, exist
}

// Account Struct
type Account struct {
	User
	Profile
}

func (account *Account) GetObject(userId uint) Account {
	database.DB.Model(&User{}).
		Joins("left join profiles on profiles.user_id=users.id", database.DB.Where("users.id = ?", userId)).
		Scan(account)
	database.DB.Model(&Profile{}).
		Joins("left join users on users.id=profiles.user_id", database.DB.Where("users.id = ?", userId)).
		Scan(account)

	return *account
}

func (account *Account) ToJSON(ctx *fiber.Ctx) fiber.Map {

	urlImage := utils.GetFileStatic(ctx, "users", account.Thumbnail)

	return fiber.Map{
		"username":  account.Username,
		"email":     account.Email,
		"uuid":      account.User.UUID,
		"phone":     account.Phone,
		"address":   account.Address,
		"address_2": account.Address2,
		"thumbnail": fiber.Map{
			"url_image":  urlImage,
			"name_image": account.Thumbnail,
		},
	}
}

func UpdateAccount(user User, profile Profile, userProfileForm forms.UserProfileForm) {
	database.DB.
		Model(&user).
		Where("id = ?", user.ID).
		UpdateColumns(User{
			Username: userProfileForm.Username,
			Email:    userProfileForm.Email,
		})
	database.DB.
		Model(&profile).
		Where("user_id = ?", profile.UserID).
		UpdateColumns(Profile{
			Address:   userProfileForm.Address,
			Address2:  userProfileForm.Address2,
			Thumbnail: userProfileForm.Thumbnail,
			Phone:     userProfileForm.Phone,
		}).Commit()
}
