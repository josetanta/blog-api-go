package models

import (
	"gitlab.com/josetanta/blog-api/database"
	"gitlab.com/josetanta/blog-api/utils"
)

type IRole interface {
	HasPermission(permission uint) bool
	GenerateRoles()
}

type Role struct {
	ID          uint   `gorm:"primaryKey"`
	NameRole    string `gorm:"unique"`
	Permissions uint

	Users []User `gorm:"foreignKey:RoleID" json:"-"`
}

func (role *Role) HasPermission(permission uint) bool {

	return role.Permissions&permission == permission
}

func (role *Role) GenerateRoles() {
	var existRoles []Role

	database.DB.Find(&existRoles)
	var roles = []Role{
		{
			NameRole:    "User",
			Permissions: utils.COMMENT | utils.WRITE | utils.FOLLOW,
		}, {
			NameRole:    "Moderator",
			Permissions: utils.MODERATOR | utils.FOLLOW,
		}, {
			NameRole:    "Admin",
			Permissions: utils.ADMINISTER,
		},
	}
	if len(existRoles) >= 3 {
		return
	}

	database.DB.CreateInBatches(roles, 3)
}
