package models

import (
	"strconv"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"gitlab.com/josetanta/blog-api/database"
	"gorm.io/gorm"

	utils2 "gitlab.com/josetanta/blog-api/utils"
)

type IUser interface {
	GetObject(params map[string]string) User
	HasToken(token string) bool
	Can(permission uint) bool
	GetToken() string
	SetSigned(value bool)
	CreateToken(token string)
	ToJSON(ctx *fiber.Ctx) fiber.Map
	VerifyToken() bool
	GenerateToken(field uint) (string, error)
}
type User struct {
	gorm.Model `json:"-"`
	Username   string `json:"username" gorm:"unique,length:20"`
	Email      string `json:"email" gorm:"unique"`
	Deleted    bool   `json:"deleted"`
	Signed     bool   `json:"signed" gorm:"default:false"`
	Password   []byte `json:"-"`
	UUID       string `json:"uuid" gorm:"index:idx_user_uuid,unique"`

	// Role
	RoleID uint `json:"-"`

	// Relations Ships
	Posts    []Post    `gorm:"foreignKey:UserID" json:"-"`
	Comments []Comment `gorm:"foreignKey:UserID" json:"-"`
}

func (user *User) AfterCreate(tx *gorm.DB) (err error) {
	profile := Profile{
		UserID: uint(int(user.Model.ID)),
	}
	tx.Create(&profile).Commit()
	return
}

func (user *User) BeforeCreate(tx *gorm.DB) (err error) {
	user.UUID = utils.UUIDv4()
	if user.Email == "jose.tanta.27@unsch.edu.pe" {
		tx.Model(user).Where("id = ?", user.ID).Update("role_id", 3)
	}
	return
}

func (user *User) GetObject(option map[string]string) (User, bool) {
	var exist = false

	userId, _ := strconv.Atoi(option["userId"])
	uuid := option["uuid"]
	if len(option["userId"]) > 0 || len(uuid) > 0 {
		database.DB.Where("id = ?", userId).Or("uuid = ?", uuid).First(user)
	}
	email := option["email"]
	if len(email) > 0 {
		database.DB.Where("email = ?", email).First(user)
	}

	if user.ID != 0 {
		exist = true
	}

	return *user, exist
}

func (user *User) HasToken(token string) bool {
	var userToken UserToken
	database.DB.Where("token = ?", token).First(&userToken)

	return user.ID == userToken.UserID
}

func (user *User) Can(permission uint) bool {
	var role Role
	database.DB.Where("id = ?", user.RoleID).First(&role)

	return role.HasPermission(permission)
}

func (user *User) GetToken() string {
	var userToken UserToken

	database.DB.Where("user_id = ?", user.ID).First(&userToken)

	return userToken.Token
}
func (user *User) DeleteToken() {
	var userToken UserToken

	database.DB.Where("user_id = ?", user.ID).First(&userToken)

	userToken.DeleteToken(user.ID)
}

func (user *User) SetSigned(value bool) {
	database.DB.Model(user).Where("id = ?", user.ID).Update("signed", value).Commit()
}

func (user *User) SaveUserToken(token string) {

	userToken := UserToken{
		UserID:    user.ID,
		Token:     token,
		ExpiredIn: utils2.ExpiredIn,
	}
	database.DB.Save(&userToken).Commit()
}

func (user *User) ToJSON(ctx *fiber.Ctx) fiber.Map {
	var account = new(Account).GetObject(user.ID)
	imageUrl := utils2.GetFileStatic(ctx, "users", account.Thumbnail)

	return fiber.Map{
		"username": account.Username,
		"email":    account.Email,
		"thumbnail": fiber.Map{
			"url_image":  imageUrl,
			"name_image": account.Thumbnail,
		},
		"relations": fiber.Map{
			"posts": PostGetObjectsByUserId(ctx, user.ID, "created_at desc"),
		},
	}
}

func GenerateToken(field uint) (string, error) {
	var (
		t string
		e error
	)
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    strconv.Itoa(int(field)),
		ExpiresAt: utils2.ExpiredIn.Unix(),
	})
	t, e = (claims).SignedString([]byte(utils2.SecretKey))
	return t, e
}

func VerifyToken(t string) bool {
	token, err := jwt.ParseWithClaims(t, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(utils2.SecretKey), nil
	})

	if err != nil {
		return false
	}

	claims := token.Claims.(*jwt.StandardClaims)
	var user User
	database.DB.Where("id = ?", claims.Issuer).First(&user)

	return user.ID != 0
}

func GetIssuer(ctx *fiber.Ctx) uint {
	t := utils2.GetToken(ctx)
	token, err := jwt.ParseWithClaims(t, &jwt.StandardClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(utils2.SecretKey), nil
	})

	if err != nil {
		return 0
	}

	claims := token.Claims.(*jwt.StandardClaims)
	var user User
	database.DB.Where("id = ?", claims.Issuer).First(&user)
	id, _ := strconv.Atoi(claims.Issuer)
	return uint(id)
}
