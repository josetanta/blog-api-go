package models

import (
	"strconv"
	"time"

	"gitlab.com/josetanta/blog-api/database"
)

type IUserToken interface {
	DeleteToken(userId string)
	Me(token string) UserToken
	GetUser() User
}

type UserToken struct {
	ID        uint      `gorm:"primaryKey"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
	Token     string    `json:"token" gorm:"unique"`
	ExpiredIn time.Time `json:"-"`

	UserID uint
	// User   `gorm:"constraint:onDelete:CASCADE;" json:"-"`
}

func (userToken *UserToken) GetObject(token string) (UserToken, User, bool) {
	var exist = false
	if len(token) > 0 {
		database.DB.Where("token = ?", token).First(userToken)
	}

	if userToken.ID != 0 {
		exist = true
	}
	user, existUser := userToken.getUser()
	return *userToken, user, exist && existUser
}

func (userToken *UserToken) DeleteToken(userId uint) {
	database.DB.Where("user_id = ?", userId).Delete(userToken).Commit()
}

func (userToken *UserToken) getUser() (User, bool) {
	var user, exist = new(User).GetObject(map[string]string{"userId": strconv.Itoa(int(userToken.UserID))})

	return user, exist
}
