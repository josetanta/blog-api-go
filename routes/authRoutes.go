package routes

import (
	"os"

	"github.com/gofiber/fiber/v2"
	apiV1 "gitlab.com/josetanta/blog-api/controllers/api"
	"gitlab.com/josetanta/blog-api/utils"

	"github.com/markbates/goth"
	"github.com/markbates/goth/providers/github"
	"github.com/markbates/goth/providers/google"
	"github.com/markbates/goth/providers/spotify"
)

func AuthRoutes(route fiber.Router) {

	utils.LoadEnvironment()

	var (
		googleClientID  = os.Getenv("GOOGLE_CLIENT_ID")
		googleSecretKey = os.Getenv("GOOGLE_SECRET_KEY")

		githubClientID  = os.Getenv("GITHUB_CLIENT_ID")
		githubSecretKey = os.Getenv("GITHUB_SECRET_KEY")

		spotifyClientID  = os.Getenv("SPOTIFY_CLIENT_ID")
		spotifySecretKey = os.Getenv("SPOTIFY_SECRET_KEY")

		redirectCallback = os.Getenv("REDIRECT_CALLBACK")
	)

	goth.UseProviders(
		google.New(googleClientID, googleSecretKey, redirectCallback, "email", "profile"),
		github.New(githubClientID, githubSecretKey, redirectCallback, "email", "profile"),
		spotify.New(spotifyClientID, spotifySecretKey, redirectCallback, "user-read-email"),
	)

	route.Get("/login/auth/:provider", apiV1.LoginAuth)
	route.Get("/auth/callback", apiV1.LoginAuthUser)
	route.Get("/auth/callback/:provider", apiV1.LoginAuthUser)
	route.Post("/auth/logout/:provider", apiV1.LogoutAuth)
	route.Get("/auth/token/", Middle["hasToken"], apiV1.GetTokenUser)

	route.Post("/login/", apiV1.UserLogin)
	route.Post("/register/", apiV1.UserRegister)
	route.Post("/logout/", Middle["hasToken"], apiV1.UserLogout)
}
