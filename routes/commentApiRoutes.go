package routes

import (
	"github.com/gofiber/fiber/v2"
	api "gitlab.com/josetanta/blog-api/controllers/api"
)

func CommentApiRoutes(route fiber.Router) {
	// Comments - v1
	commentApi := route.Group("/comments")
	commentApi.Post("/:postId", Middle["hasToken"], Middle["permissions"], api.CreateComment)
	commentApi.Patch("/:commentId", Middle["hasToken"], Middle["permissions"], Middle["isOwnerComment"], api.UpdateComment)
	commentApi.Delete("/:commentId", Middle["hasToken"], Middle["permissions"], Middle["isOwnerComment"], api.DeleteComment)
}
