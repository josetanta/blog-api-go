package routes

import (
	"github.com/gofiber/fiber/v2"
	api "gitlab.com/josetanta/blog-api/controllers/api"
)

func PostApiRoutes(route fiber.Router) {
	// Posts - v1
	postApi := route.Group("/posts")
	postApi.Get("/", api.GetPostList)
	postApi.Get("/deletes/", Middle["hasToken"], Middle["permissions"], api.PostDeletedForUser)
	postApi.Post("/deleted/", Middle["hasToken"], Middle["permissions"], api.PostDeletedForUser)
	postApi.Delete("/deleted/", Middle["hasToken"], Middle["permissions"], api.PostDeletedForUser)
	postApi.Get("/search/", api.SearchPosts)
	postApi.Get("/:postId", api.GetPostSingle)
	postApi.Post("/", Middle["hasToken"], Middle["permissions"], api.CreatePost)
	postApi.Delete("/:postId", Middle["hasToken"], Middle["permissions"], Middle["isOwnerPost"], api.DeletePost)
	postApi.Patch("/:postId", Middle["hasToken"], Middle["permissions"], Middle["isOwnerPost"], api.UpdatePost)
}
