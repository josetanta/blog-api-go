package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/basicauth"

	"gitlab.com/josetanta/blog-api/middlewares"
)

var Middle = map[string]fiber.Handler{
	"hasToken":       basicauth.New(middlewares.HasTokenMiddleware),
	"permissions":    basicauth.New(middlewares.PermissionMiddleware),
	"isOwnerPost":    basicauth.New(middlewares.VerifyOwnerPostMiddleware),
	"isOwnerComment": basicauth.New(middlewares.VerifyOwnerCommentMiddleware),
}

func SetupRoutesAPI(app *fiber.App) {
	api := app.Group("/api")

	api.Get("/message", func(ctx *fiber.Ctx) error {
		return ctx.Status(fiber.StatusOK).JSON("Ok")
	})

	api.Get("/health", func(ctx *fiber.Ctx) error {
		return ctx.SendStatus(fiber.StatusOK)
	})

	// Static files
	api.Static("/static", "./static/public/images")

	// Auth
	AuthRoutes(api)

	//	User
	UserApiRoutes(api)

	//	Post
	PostApiRoutes(api)

	//	Comment
	CommentApiRoutes(api)

}
