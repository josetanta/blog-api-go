package routes

import (
	"github.com/gofiber/fiber/v2"
	api "gitlab.com/josetanta/blog-api/controllers/api"
)

func UserApiRoutes(route fiber.Router) {
	// User v1
	userApi := route.Group("/users")
	userApi.Get("/account/", Middle["hasToken"], api.GetUserAccount)
	userApi.Get("/:userId/details/", api.GetUserDetails)
	userApi.Patch("/account/", Middle["hasToken"], api.UpdateUserAccount)
	userApi.Delete("/account/", Middle["hasToken"], Middle["permissions"], api.DeleteAccount)
}
