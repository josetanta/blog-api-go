package utils

import (
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"mime/multipart"
	"os"
	"strings"

	"path/filepath"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	"github.com/nfnt/resize"
)

// Return nombre de la image y la existencia
// de la imagen
func SaveFile(ctx *fiber.Ctx, model string) (string, bool) {
	nameNewFile := model + utils.UUIDv4()[0:23]

	var (
		exist    = false
		pathFile string
	)

	pathSave := "./static/public/images/" + model + "/%s"

	if form, err := ctx.MultipartForm(); err == nil {
		files := form.File["image"]

		for _, file := range files {

			if file != nil {
				exist = true
				extension := filepath.Ext(file.Filename)
				file.Filename = nameNewFile + extension
				resizeFile(file, pathSave, model)
				pathFile = file.Filename
			}
		}
	}
	return pathFile, exist
}

func resizeFile(file *multipart.FileHeader, path, model string) {
	openFile, _ := file.Open()
	img, _, _ := image.Decode(openFile)
	extension := filepath.Ext(file.Filename)
	var resizeImage image.Image

	if model == "users" {
		resizeImage = resize.Resize(460, 0, img, resize.Lanczos3)
	} else if model == "posts" {
		resizeImage = resize.Resize(1024, 0, img, resize.Lanczos3)
	}

	out, _ := os.Create(fmt.Sprintf(path, file.Filename))

	defer out.Close()

	if extension == ".jpg" || extension == ".jpeg" {
		jpeg.Encode(out, resizeImage, nil)
	} else if extension == ".png" {
		png.Encode(out, resizeImage)
	}
}

func GetFileStatic(ctx *fiber.Ctx, model string, field string) string {

	if strings.Contains(field, "http") || strings.Contains(field, "https") {
		return field
	} else {
		baseUrl := ctx.BaseURL()
		fileUrl := baseUrl + "/api/static/" + model + "/" + field
		return fileUrl
	}

}

func RemoveFileStatic(model string, nameImage string) bool {
	pathOriginal := "./static/public/images/" + model + "/" + nameImage

	if err := os.Remove(pathOriginal); err != nil {
		return false
	}
	return true
}
