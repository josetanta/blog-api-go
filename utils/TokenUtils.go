package utils

import (
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
)

var timer = 48 * time.Hour //  48 * time.Hour - 2 days

const SecretKey = "03a4675222b3a8f513d4eab44cf111c44a585bd3cdbd1ef608fd3db795575063"

var ExpiredIn = time.Now().Add(timer).Local()

func IsExpiredVerify(timeExpired time.Time) bool {
	today := time.Now().Local()

	return timeExpired.Before(today)
}

func GetToken(ctx *fiber.Ctx) string {
	var (
		schemaToken string
		token       string
	)

	headerAuth := ctx.Get("Authorization")
	tokenCookie := ctx.Cookies("jwt")

	if len(headerAuth) > 0 {
		headerSplit := strings.Split(headerAuth, " ")
		schemaToken = headerSplit[0]
		if len(headerSplit) > 1 && len(schemaToken) > 0 && schemaToken == "Bearer" {
			token = (headerSplit[1])
		} else {
			token = ""
		}
	} else if len(tokenCookie) > 0 {
		token = tokenCookie
	}

	return token
}
