package utils

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

func LoadEnvironment() {
	env := os.Getenv("APP_ENV")

	if env == "development" || env == "" {
		godotenv.Load()
		fmt.Println("Load mode development")
	} else if env == "production" {
		godotenv.Load()
		fmt.Println("Load Production")
	}

}
